public class BlockPaccoDoor : BlockDoorSecure
{
    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_blockValue.ischild)
        {
            Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
            BlockValue block = _world.GetBlock(parentPos);
            OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, parentPos, block, _player);
        }
        TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_cIdx, _blockPos);
        if (tileEntitySecureDoor == null)
        {
            if (!_world.IsEditor())
            {
                return false;
            }
        }
        bool locked = false;
        if (!_world.IsEditor())
        {
            locked = tileEntitySecureDoor.IsLocked();
        }
        else
        {
            locked = ((_blockValue.meta & 4) != 0);
        }
        bool isAllowed = false;
        if (!_world.IsEditor())
        {
            // check if the user has a certain quest completed
            //isAllowed = tileEntitySecureDoor.IsUserAllowed(GamePrefs.GetString(EnumGamePrefs.PlayerId));
            isAllowed = true;
            Quest questExists = (_player as EntityPlayerLocal).QuestJournal.FindQuest("pacco_game_room");
            if (questExists == null || questExists.Active) isAllowed = false;
        }
        else
        {
            isAllowed = false;
        }
        switch (_indexInBlockActivationCommands)
        {
            case 0:
                if (!_world.IsEditor())
                {
                    if (locked)
                    {
                        if (!isAllowed)
                        {
                            Audio.Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "locked");
                            GameManager.ShowTooltip(_player as EntityPlayerLocal, "You haven't gained Paccos trust yet...");
                            return false;
                        }
                    }
                }
                return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
            case 1:
                if (!_world.IsEditor())
                {
                    if (locked)
                    {
                        if (!isAllowed)
                        {
                            Audio.Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "locked");
                            GameManager.ShowTooltip(_player as EntityPlayerLocal, "You haven't gained Paccos trust yet...");
                            return false;
                        }                        
                    }
                }
                return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
            case 2:
                if (_world.IsEditor())
                {
                    _blockValue.meta |= 4;
                    _world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
                }
                else
                {
                    tileEntitySecureDoor.SetLocked(true);
                }
                Audio.Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "locking");
                //GameManager.ShowTooltip(_player as EntityPlayerLocal, VX.HH(125267));
                return true;
            case 3:
                if (_world.IsEditor())
                {
                   _blockValue.meta = (byte)((int)_blockValue.meta & -5);
                    _world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
                }
                else
                {
                    tileEntitySecureDoor.SetLocked(false);
                }
                Audio.Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "unlocking");
                //GameManager.ShowTooltip(_player as EntityPlayerLocal, VX.HH(125317));
                return true;
            case 4:
                {
                    LocalPlayerUI uIForPlayer = LocalPlayerUI.GetUIForPlayer(_player as EntityPlayerLocal);
                    if (uIForPlayer != null)
                    {
                       uIForPlayer.windowManager.Open(GUIWindowKeypad.ID, true, false, true);
                    }
                    NGuiKeypad.Instance.LockedItem = tileEntitySecureDoor;
                    return true;
                }
            default:
                return false;
        }
    }
}