﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Random = System.Random;

public class BlockRandomizer : Block
{
    private bool disableDebug = true;
    private string objectName = "";
    private string rootName = "";

    public override void Init()
    {
        base.Init();
        if (this.Properties.Values.ContainsKey("objectName"))
        {
            objectName = this.Properties.Values["objectName"];            
        }
        if (this.Properties.Values.ContainsKey("rootName"))
        {
            rootName = this.Properties.Values["rootName"];
        }
    }

    // this happens anytime blockValue changes
    public override void OnBlockValueChanged(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _oldBlockValue,
        BlockValue _newBlockValue)
    {
        base.OnBlockValueChanged(_world, _clrIdx, _blockPos, _oldBlockValue, _newBlockValue);
        if (GameManager.IsDedicatedServer) return; // no need to do anything related to this if it's the server
        BlockEntityData _ebcd = _world.ChunkClusters[_clrIdx].GetBlockEntity(_blockPos);
        if (_ebcd == null) return; // not possible to get block entity data
        if (_ebcd.transform == null) return; // not possible to get a gameobject transform to do the randomization
        if (_newBlockValue.meta != _oldBlockValue.meta || _newBlockValue.meta2 != _oldBlockValue.meta2 ||
            _newBlockValue.meta3 != _oldBlockValue.meta3)
            ShowObjects(_ebcd, _newBlockValue); // will only run if any of the metas change, so it should only happen on instanciation
    }

    // this happens when a player loads the scean, so that the object syncs to the correct state
    public override void ForceAnimationState(BlockValue _blockValue, BlockEntityData _ebcd)
    {
        base.ForceAnimationState(_blockValue, _ebcd);
        if (GameManager.IsDedicatedServer) return; // no need to do anything related to this if it's the server
        if (_ebcd == null) return; // not possible to get block entity data
        if (_ebcd.transform == null) return; // not possible to get a gameobject transform to do the randomization
        ShowObjects(_ebcd, _blockValue);
    }

    // when the block is added to the world, it will randomize the objects to show
    public override void OnBlockAdded(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
    {
        base.OnBlockAdded(_world, _chunk, _blockPos, _blockValue);
        if (_world.IsRemote()) return; // only the server (or a local game) needs to do this
        // randomize the metadata so that players see it the right way
        // since this is a normal block, i can use up to 12 bits (all meta available)
        // that means we can use up to 12 different objects only
        // using bits instead of an integer number, allows us to show multiple gems at the same time
        // so we choose a random number of 1 to max objects which will tell us how many objects will be shown
        // then we randomize which objects will be shown from the list
        int numberToShow = 0;
        if (this.Properties.Values.ContainsKey("ShownInterval"))
        {
            // there is an interval configured to detemine the number of objects to show
            string intervalStr = this.Properties.Values["ShownInterval"];
            string[] parts = intervalStr.Split(',');
            int minNum = 1;
            int maxNum = 1;
            if (parts.Length == 1)
            {
                minNum = maxNum = Convert.ToInt32(parts[0]);
            }
            else if (parts.Length == 2)
            {
                minNum = Convert.ToInt32(parts[0]);
                minNum = Convert.ToInt32(parts[1]);
            }
            if (maxNum > 12) maxNum = 12;
            numberToShow = UnityEngine.Random.Range(minNum, maxNum);
        }
        if (this.Properties.Classes.ContainsKey("ObjectList"))
        {
            DynamicProperties dynamicProperties = this.Properties.Classes["ObjectList"];
            int objectNumber = dynamicProperties.Values.Keys.Count; // number of configured objects
            if (objectNumber < numberToShow) numberToShow = objectNumber; // if there are fewer object then we actually want to show)
            List<int> objectsToShow = new List<int>();
            int oIndex = 0;
            _blockValue.meta = 0; // bits 0 to 3
            _blockValue.meta2 = 0; // bits 4 to 7
            _blockValue.meta3 = 0; // bits 8 to 11
            while (numberToShow > 0)
            {
                oIndex = UnityEngine.Random.Range(0, objectNumber);
                if (!objectsToShow.Contains(oIndex))
                {
                    objectsToShow.Add(oIndex);
                    if (oIndex <= 3)
                    {
                        _blockValue.meta = (byte)(_blockValue.meta | (1 << oIndex));
                    }
                    else if (oIndex <= 7)
                    {
                        oIndex -= 4;
                        _blockValue.meta2 = (byte)(_blockValue.meta2 | (1 << oIndex));
                    }
                    else if (oIndex <= 11)
                    {
                        oIndex -= 8;
                        _blockValue.meta3 = (byte)(_blockValue.meta3 | (1 << oIndex));
                    }
                    // set up the metadata
                    numberToShow--;
                }
            }
            // save the metadata
            _world.SetBlockRPC(_chunk.ClrIdx, _blockPos, _blockValue);
        }        

    }
    // show the objects depending on the predefined bits on instanciation
    private void ShowObjects(BlockEntityData _ebcd, BlockValue _blockValue)
    {
        if (GameManager.IsDedicatedServer) return; // no need to do anything related to this if it's the server        
        if (this.Properties.Classes.ContainsKey("ObjectList"))
        {
            DynamicProperties dynamicProperties = this.Properties.Classes["ObjectList"];
            int oIndex = 0;
            int objectIndex = 0;
            foreach (string itemNumber in dynamicProperties.Values.Keys)
            {
                bool showObject = false;
                oIndex = objectIndex;
                if (oIndex <= 3)
                {
                    showObject = checkBit(_blockValue.meta, oIndex);
                }
                else if (oIndex <= 7)
                {
                    showObject = checkBit(_blockValue.meta2, oIndex - 4);
                }
                else if (oIndex <= 11)
                {
                    showObject = checkBit(_blockValue.meta3, oIndex - 8);
                }
                ShowHideObject(_ebcd.transform, _ebcd.transform, dynamicProperties.Values[itemNumber], showObject);
                objectIndex++;
            }
        }
    }
    // check a metadata bit
    public static bool checkBit(byte _metadata, int bitNumber)
    {
        return ((int)_metadata & 1 << bitNumber) != 0;
    }
    // show or hide a object inside the transform hierarchy
    private bool ShowHideObject(Transform root, Transform t, string partName, bool show)
    {
        if (t.name == partName)
        {
            t.gameObject.SetActive(show);
            return true;
        }
        foreach (Transform tran in t)
        {
            if (ShowHideObject(root, tran, partName, show)) return true;
        }
        return false;
    }
}